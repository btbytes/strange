# Next

- Switched to hyper 0.11 for the `serve` command
- Added Type plugin for grouping pages by type
- **BREAKING** The `template` meta-data in pages is now `type`
- **EXPERIMENTAL** Support for themes

# 0.8.1

- Published on crates.io

# 0.8.0

- Complete overhaul of rendering engine to allow modular multi-engine rendering
