/*
	Copyright 2016,2017 Alessandro Pellizzari

	This file is part of strange, a static website builder in rust.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

#![recursion_limit = "1024"]

#[macro_use]
extern crate error_chain;
#[macro_use]
extern crate lazy_static;
extern crate clap;
extern crate pulldown_cmark;
extern crate handlebars;
extern crate tera;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate serde_yaml;
extern crate regex;
extern crate time;
extern crate slug;
extern crate term;
extern crate rayon;
extern crate hyper;
extern crate futures;
extern crate notify;

#[macro_use(o, slog_log, slog_trace, slog_debug, slog_info, slog_warn, slog_error)]
extern crate slog;
#[macro_use]
extern crate slog_scope;
extern crate slog_term;

use slog::DrainExt;

mod cmd;
#[macro_use]
mod utils;
mod models;
mod plugin;
mod tpl;
mod errors;
mod config;
mod theme;

use errors::*;

// Take some variables from Cargo.toml, if available
const VERSION: Option<&'static str> = option_env!("CARGO_PKG_VERSION");
const AUTHORS: Option<&'static str> = option_env!("CARGO_PKG_AUTHORS");
//const HOMEPAGE: Option<&'static str> = option_env!("CARGO_PKG_HOMEPAGE");


fn main() {
	match run() {
		Err(e) => {
			dump_error(&e);
			::std::process::exit(1);
		}
		Ok(m) => {
			info!("{}", m);
			::std::process::exit(0);
		}
	}
}

fn run() -> Result<String> {
	// Initialize logger with basic verbosity for config and plugins loading
	prepare_logger(0);


	let matches = clap::App::new("Strange")
		.version(VERSION.unwrap_or_else(|| "unknown"))
		.author(AUTHORS.unwrap_or_else(|| "Alessandro Pellizzari <alex@amiran.it>"))
		.about("A static web-site generator")
		.setting(clap::AppSettings::SubcommandRequiredElseHelp)
		.setting(clap::AppSettings::DeriveDisplayOrder)
		.setting(clap::AppSettings::ColoredHelp)
		.setting(clap::AppSettings::ColorAuto)
		.setting(clap::AppSettings::VersionlessSubcommands)
		.arg(clap::Arg::with_name("v")
			.short("v")
			.multiple(true)
			.help("Sets the level of verbosity")
		)
		.subcommand(clap::SubCommand::with_name("init")
			.about("Create a new site in the current directory. If a name is provided, create a directory with that name for the site.")
			.setting(clap::AppSettings::ColoredHelp)
			.setting(clap::AppSettings::ColorAuto)
			.arg(clap::Arg::with_name("engine")
				.short("e")
				.long("engine")
				.takes_value(true)
				.value_name("ENGINE")
				.help("Select the templating engine to use: tera or handlebars")
			)
			.arg(clap::Arg::with_name("source")
				.short("s")
				.long("source")
				.takes_value(true)
				.value_name("SOURCEDIR")
				.help("Set the dir containing sources")
			)
			.arg(clap::Arg::with_name("dest")
				.short("d")
				.long("dest")
				.takes_value(true)
				.value_name("DESTDIR")
				.help("Set the dir containing the built pages")
			)
			// TODO: theme
			.arg(clap::Arg::with_name("NAME")
				.help("Optional name of a directory to create for the site")
				.required(false)
				.index(1)
			)
		)
		.subcommand(clap::SubCommand::with_name("config")
			.about("Manage the configuration")
			.setting(clap::AppSettings::ColoredHelp)
			.setting(clap::AppSettings::ColorAuto)
			.subcommand(clap::SubCommand::with_name("update")
				.about("Updates the config file keeping the current settings and adding default values for new options. OVERWRITES THE CONFIG FILE!")
				.setting(clap::AppSettings::ColoredHelp)
				.setting(clap::AppSettings::ColorAuto)
			)
		)
		.subcommand(clap::SubCommand::with_name("new")
			.about("Create a new empty article with the given title.")
			.setting(clap::AppSettings::TrailingVarArg)
			.setting(clap::AppSettings::ColoredHelp)
			.setting(clap::AppSettings::ColorAuto)
			.arg(clap::Arg::with_name("date")
				.short("d")
				.long("date")
				.help("Prepend date in Y-m-d format to file name")
			)
			.arg(clap::Arg::with_name("time")
				.short("t")
				.long("time")
				.requires("date")
				.help("Add time to date in filename and prelude (doesn't work without -d)")
			)
			.arg(clap::Arg::with_name("path")
				.short("p")
				.long("path")
				.help("Create the article in the specified (relative) sub-path")
				.takes_value(true)
				.value_name("PATH")
			)
			.arg(clap::Arg::with_name("TITLE")
				.help("Title of the page")
				.takes_value(true)
				.required(true)
				.multiple(true)
			)
		)
		.subcommand(clap::SubCommand::with_name("build")
			.setting(clap::AppSettings::ColoredHelp)
			.setting(clap::AppSettings::ColorAuto)
			.about("Builds all the src CommonMark-formatted files to html files in build.")
		)
		.subcommand(clap::SubCommand::with_name("watch")
			.setting(clap::AppSettings::ColoredHelp)
			.setting(clap::AppSettings::ColorAuto)
			.about("Watches the source dir for changes and automatically rebuild the pages.")
		)
		.subcommand(clap::SubCommand::with_name("serve")
			.setting(clap::AppSettings::ColoredHelp)
			.setting(clap::AppSettings::ColorAuto)
			.about("Starts a webserver on 127.0.0.1:9876 to serve the generated pages.")
			.arg(clap::Arg::with_name("port")
				.short("p")
				.long("port")
				.takes_value(true)
				.value_name("port")
				.help("Select the TCP port the webserver will be listening on. Default: 9876")
			)
			.arg(clap::Arg::with_name("ip")
				.short("a")
				.long("address")
				.takes_value(true)
				.value_name("ip")
				.help("Select the IP the webserver will be listening on. Default: 127.0.0.1")
			)
		)
		.subcommand(clap::SubCommand::with_name("clean")
			.setting(clap::AppSettings::ColoredHelp)
			.setting(clap::AppSettings::ColorAuto)
			.about("Cleans the build directory.")
		)
		.subcommand(clap::SubCommand::with_name("theme")
			.setting(clap::AppSettings::ColoredHelp)
			.setting(clap::AppSettings::ColorAuto)
			.about("Manage themes.")
			.subcommand(clap::SubCommand::with_name("info")
				.about("Get info about the current theme")
				.setting(clap::AppSettings::ColoredHelp)
				.setting(clap::AppSettings::ColorAuto)
			)
		)
		.get_matches_safe()
		.unwrap_or_else(|e| {
			println!("{}", e);
			::std::process::exit(0);
		});
	;

	// Re-initialize logger with verbosity from command line
	let verbosity = matches.occurrences_of("v");
	prepare_logger(verbosity);

	info!("Application started");

	match matches.subcommand() {
		("init", matches) => {
			matches
				.ok_or_else(|| "Missing init parameters".into())
				.and_then(|matches| {
					let mut config = config::read_config()
						.unwrap_or_else(|_| {
							config::Config::default()
						});

					matches.value_of("engine").map(|v| config.templates.engine = v.to_string());
					matches.value_of("source").map(|v| config.paths.src = v.to_string());
					matches.value_of("dest").map(|v| config.paths.dest = v.to_string());

					cmd::init::run(
						&config,
						matches.value_of("NAME")
					)
				})
		}
		("config", matches) => {
			match matches {
				Some(subcommand) => {
					match subcommand.subcommand_name() {
						Some("update") => {
							let mut config = config::read_config()?;
							config.plugins.rescan();
							debug!("Config: {:?}", config);
							config::write_config(&config)?;
							Ok("Config updated".into())
						},
						_ => {
							println!("{}", subcommand.usage());
							Ok("Please specify sub-command".into())
						}
					}
				}
				None => unimplemented!()
			}
		}
		("new", matches) => {
			matches
				.ok_or_else(|| "Missing parameters".into())
				.and_then(|matches| {
					let mut config = config::read_config()?;

					matches.value_of("date").map(|_| {config.pages.date = true;});
					matches.value_of("time").map(|_| {config.pages.time = true;});

					cmd::new::run(
						&config,
						&matches.values_of("TITLE").unwrap().collect::<Vec<&str>>().join(" "),
						matches.value_of("path").unwrap_or("")
					)
				})
		}
		("build", _) => {
			let config = config::read_config()?;
			cmd::build::run(&config)
		}
		("watch", _) => {
			let config = config::read_config()?;
			cmd::watch::run(&config)
		}
		("serve", matches) => {
			matches
				.ok_or_else(||"Missing parameters".into())
				.and_then(|matches| {
					let mut config = config::read_config()?;

					matches.value_of("ip").map(|v| config.serve.ip = v.to_string());
					matches.value_of("port").map(|v| config.serve.port = v.to_string());

					cmd::serve::run(&config)
				})
		}
		("clean", _) => {
			let config = config::read_config()?;
			cmd::clean::run(&config)
		}
		("theme", matches) => {
			match matches {
				Some(subcommand) => {
					match subcommand.subcommand_name() {
						Some("info") => {
							let config = config::read_config()?;
							cmd::theme::info(&config)
						},
						_ => {
							println!("{}", subcommand.usage());
							Ok("Please specify sub-command".into())
						}
					}
				}
				None => unimplemented!()
			}
		}
		_ => {
			Err("Unexpected error.".into())
		}
	}
}

fn prepare_logger(verbosity: u64) {
	let log_level = match verbosity {
		0 => slog::Level::Info,
		1 => slog::Level::Debug,
		_ => slog::Level::Trace,
	};

	let term_streamer = slog_term::streamer().full().use_custom_timestamp(|_| { Ok(()) }).build();
	let drain = slog::level_filter(log_level, term_streamer).fuse();
	let logger = slog::Logger::root(drain, o!());
	slog_scope::set_global_logger(logger);
}
