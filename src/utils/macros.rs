/*
	Copyright 2017 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

#[macro_export]
macro_rules! btree {
    ( $( $k:expr => $v:expr ),* ) => ({
        let mut temp = BTreeMap::new();
        $(
            temp.insert($k.to_owned(), $v);
        )*
        temp
    });
    ( $( $k:expr => $v:expr ),+, ) => (btree!( $($k => $v),+ ))
}
