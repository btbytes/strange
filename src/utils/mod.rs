/*
	Copyright 2017 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use regex::Regex;
use time;

lazy_static! {
	pub static ref DATEVAL_RE : Regex = Regex::new(r"(?imx)^
		(?P<y>\d{4})
		-
		(?P<m>[01][0-9])
		-
		(?P<d>[0-3][0-9])
		(
			[t ]
			(?P<h>[0-2][0-9])
			:
			(?P<i>[0-5][0-9])
			(
				(
					:
					(?P<s>[0-5][0-9])
				)
				([z+-].*)?
			)?
		)?").expect("Cannot prepare dateval regex");
}

pub mod files;
pub mod print;

#[macro_use]
pub mod macros;

pub fn time_diff_in_ms(start: &time::Timespec, end: &time::Timespec) -> u64 {
	let s = start.sec as f64 + (f64::from(start.nsec) / 1_000_000_000__f64);
	let e = end.sec as f64 + (f64::from(end.nsec) / 1_000_000_000__f64);
	((e-s) * 1_000__f64) as u64
}
