/*
	Copyright 2017 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::fs;
use std::path::Path;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;

use errors::*;

pub fn read_to_string<P>(filename: P) -> Result<String>
	where P: AsRef<Path>
{
	let filename = filename.as_ref();
	let mut file_contents = String::new();

	BufReader::new(File::open(filename)?)
		.read_to_string(&mut file_contents)
		.chain_err(|| format!("Unable to read from file '{}'", filename.display()))?;

	Ok(file_contents)
}

pub fn read_part<P>(filename: P, size: usize) -> Result<Vec<u8>>
	where P: AsRef<Path>
{
	let filename = filename.as_ref();

	let mut buffer = Vec::with_capacity(size);
	let reader = BufReader::new(File::open(filename)?);
	reader.take(size as u64).read_to_end(&mut buffer)?;

	Ok(buffer)
}


pub fn read_binary<P>(filename: P) -> Result<Vec<u8>>
	where P: AsRef<Path>
{
	let filename = filename.as_ref();
	let mut file_contents = Vec::new();

	BufReader::new(File::open(filename)?)
		.read_to_end(&mut file_contents)
		.chain_err(|| format!("Unable to read from file '{}'", filename.display()))?;

	Ok(file_contents)
}


pub fn write_to_file<P>(filename: P, contents: &str) -> Result<()>
	where P: AsRef<Path>
{
	let filename = filename.as_ref();
	fs::File::create(filename)
		.and_then(|mut dest_file| dest_file.write_all(contents.as_bytes()))
		.chain_err(|| format!("Unable to write to file '{}'", filename.display()))
}

pub fn copy_file<PS, PD>(source: PS, dest: PD) -> Result<u64>
	where PS: AsRef<Path>,
		  PD: AsRef<Path>
{
	let source = source.as_ref();
	let dest = dest.as_ref();
	let parent = dest.parent()
		.ok_or_else(|| ErrorKind::Error("Destination path has no parent directory".into()))?;

	fs::create_dir_all(parent)
		.and_then(|_| fs::copy(source, dest))
		.chain_err(|| "Unable to copy file")
}

pub fn is_filename_not_allowed(file_name: &str) -> bool {
	file_name.is_empty()
		|| file_name.starts_with('.')
		|| file_name.starts_with('_')
		|| file_name.ends_with('~')
		|| file_name.ends_with(".bak")
}
