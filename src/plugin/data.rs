/*
	Copyright 2017 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use serde_json::Value;

use serde_yaml::from_str as yaml_from_str;

use std::{fs, env};
use std::path::{Path, PathBuf};

use errors::*;
use super::{Plugin, PluginResult, Object};
use models::Directory;
use utils::files;
use config::Config;

pub struct DataPlugin;

impl Plugin for DataPlugin {
	fn name(&self) -> String {
		"Data".to_string()
	}

	fn config_defaults(&self) -> Option<Object>
	{
		let mut defaults = Object::new();
		defaults.insert("dir".into(), "data".into());
		Some(defaults)
	}

	fn post_init(&self, config: &Config, base_dir: &Path) -> PluginResult
	{
		let full_path = base_dir.join(
			&config.plugins.get_string(&self.id(), "dir")
				.unwrap_or_else(|| "data".to_string())
		);

		if full_path.exists() {
			return Ok(Some("Data dir already exists".into()));
		}

		fs::create_dir_all(&full_path).chain_err(|| {
			format!(
				"Unable to create requested data directory '{}'",
				full_path.to_str().unwrap_or_else(|| "[invalid filename]")
			)
		})?;

		Ok(Some("Data dir created".into()))
	}

	fn process_docs(&self, _: &Config, _: &Directory) -> PluginResult {
		let mut absolute_path = env::current_dir()?;
		absolute_path.push(PathBuf::from("data"));
		let absolute_path = absolute_path.as_path();

		if !absolute_path.exists() || !absolute_path.is_dir() {
			warn!("Path {} does not exist", absolute_path.to_str().unwrap());
			return Ok(None)
		}

		let mut data = Object::new();

		let paths = fs::read_dir(absolute_path)?;
		let mut unknowns = 0;
		for dir_entry in paths {
			match dir_entry {
				Ok(d) => {
					let path = d.path();

					if path.is_dir() {
						continue;
					}

					let file_name = match path.file_name() {
						Some(n) => match n.to_str() {
							Some(n) => n.to_string(),
							None => "".to_string()
						},
						None => "".to_string()
					};

					if file_name == "" || file_name.starts_with('.') {
						continue;
					}

					let extension = match path.extension() {
						Some(e) => e.to_str().unwrap_or_else(|| "").to_string(),
						None => "".to_string()
					};

					let base_name = match path.file_stem() {
						Some(n) => n.to_str().unwrap_or_else(|| "decoding_error").to_string(),
						None => {
							unknowns += 1;
							format!("unknown_{}", unknowns).to_string()
						}
					};

					if extension == "yml" || extension == "yaml" {
						let file_data = yaml_from_str::<Value>(&files::read_to_string(&path)?)
							.chain_err(|| format!("Unable to load YAML from {}", &path.display()))?;
						let file_data = file_data.as_object()
							.ok_or_else(|| ErrorKind::Plugin(self.name(), "Data YAML must represent an object".into()))?;

						data.insert(base_name, Value::Object(file_data.to_owned()));
					} else {
						continue;
					}
				}
				Err(e) => {
					error!("Error: {}", e);
				}
			}
		}

		Ok(Some(Value::Object(data)))
	}
}
