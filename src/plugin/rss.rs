/*
	Copyright 2017 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::collections::BTreeMap;
use std::path::Path;
use std::sync::Arc;
use serde_json::{Value, to_value};
use tpl::engine_tera::Tera;

use errors::*;
use super::{Object, Plugin, PluginResult};
use models::{Directory, FileMeta};
use config::Config;
use utils::files;

pub struct RssPlugin;

impl Plugin for RssPlugin {
	fn name(&self) -> String {
		"Rss".to_string()
	}

	fn config_defaults(&self) -> Option<Object>
	{
		let mut defaults = Object::new();
		defaults.insert("enabled".into(), true.into());
		defaults.insert("filename".into(), "rss.xml".into());
		defaults.insert("num".into(), 10.into());
		Some(defaults)
	}

	fn process_docs(&self, config: &Config, _: &Directory) -> PluginResult {
		let mut data = Object::new();

		data.insert(
			"url".into(),
			Value::String(format!(
				"{}/{}",
				&config.site.url,
				&config.plugins.get_string(&self.id(), "filename")
					.unwrap_or_else(|| "rss.xml".into())
			))
		);

		Ok(Some(Value::Object(data)))
	}

	fn post_build(&self, config: &Config, index: &Directory) -> PluginResult {
		if !config.plugins.get_bool(&self.id(), "enabled").unwrap_or(true) {
			return Ok(None);
		}

		let mut pagelist = BTreeMap::new();
		self.build_pagelist(index, &mut pagelist)?;
		let mut posts = pagelist
			.iter()
			.flat_map(|(_, el)| el.iter())
			.collect::<Vec<_>>();

		posts.sort_by_key(|el| el.date.iso());

		let posts = posts
			.iter()
			.rev()
			.take(config.plugins.get_int(&self.name(), "num").unwrap_or(10) as usize)
			.filter_map(|el|
				to_value(el)
					.ok()
					.and_then(|v| v.as_object().cloned())
					.map(Value::Object)
			)
			.collect::<Vec<_>>();

		let dest_path = Path::new(&config.paths.dest)
			.join(
				&config.plugins.get_string(&self.id(), "filename")
					.unwrap_or_else(|| "rss.xml".into())
			);

		let mut data = Object::new();
		data.insert("title".into(), config.site.title.clone().into());
		data.insert("url".into(), config.site.url.clone().into());
		data.insert("description".into(), config.site.description.clone().into());
		data.insert("posts".into(), posts.into());
		let rss_string = Tera::new("")
			.and_then(|tera| {
				tera.render(include_str!("files/rss.xml.twig"), &data)
			})?;

		files::write_to_file(&dest_path, &rss_string)?;

		Ok(None)
	}
}

impl RssPlugin {
	fn build_pagelist(&self, index: &Directory, pagelist: &mut BTreeMap<String, Vec<Arc<FileMeta>>>) -> Result<()> {
		for file in &index.files {
			if file.is_page() {
				let meta = file.get_meta();
				let dt = meta.date.iso();
				let list = pagelist.entry(dt).or_insert_with(Vec::new);
				list.push(meta);
			}
		}

		for dir in &index.dirs {
			self.build_pagelist(dir, pagelist)?;
		}
		Ok(())
	}
}
