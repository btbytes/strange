/*
	Copyright 2017 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use serde_json::Value;

use super::{Object, Plugin, PluginResult};
use models::Directory;
use config::Config;

pub struct ConfigPlugin;

impl Plugin for ConfigPlugin {
	fn name(&self) -> String {
		"Config".to_string()
	}

	fn process_docs(&self, config: &Config, _: &Directory) -> PluginResult {
		let mut data = Object::new();
		data.insert("title".into(), Value::from(config.site.title.to_owned()));
		data.insert("description".into(), Value::from(config.site.description.to_owned()));
		data.insert("url".into(), Value::from(config.site.url.to_owned()));

		Ok(Some(Value::Object(data)))
	}
}
