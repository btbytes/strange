/*
	Copyright 2017 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use serde_json::Value;

use super::{Object, Plugin, PluginResult};
use models::Directory;
use config::Config;

pub struct ThemePlugin;

impl Plugin for ThemePlugin {
	fn name(&self) -> String {
		"Theme".to_string()
	}

	fn process_docs(&self, config: &Config, _: &Directory) -> PluginResult {
		let mut theme_dest = config.theme.dest.clone();
		theme_dest.push_str("/");

		let mut data = Object::new();
		data.insert("path".into(), theme_dest.into());

		Ok(Some(Value::Object(data)))
	}
}
