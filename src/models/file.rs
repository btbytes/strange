/*
	Copyright 2017 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::path::{Path, PathBuf};
use std::sync::{Arc, RwLock};
use std::collections::BTreeMap;
use time;
use serde_json::{Value, to_value};
use serde_yaml::from_str as yaml_from_str;

use errors::*;
use models::{Date, Object};
use tpl::{Engines, EngineType};
use utils::{files, print, DATEVAL_RE};
use config::Config;
use plugin::PluginPageData;

#[derive(Debug, Serialize, PartialEq, Eq, Clone)]
pub enum FileType {
	Page,
	Index,
	GeneratedIndex,
	File,
}

#[derive(Debug, Serialize)]
pub struct FileMeta {
	pub title: String,
	pub name: String,
	pub url: String,
	pub tags: Vec<String>,
	pub date: Date,
	#[serde(rename = "type")]
	pub ty: String,
	file_type: FileType,
	pub author: String,
	// TODO: vec of author objects?
	pub dir: String,
	pub rootpath: String,
	pub caption: String,
	pub extra: Object,
}

impl FileMeta {
	pub fn to_object(&self) -> Object {
		to_value(self)
			.unwrap()
			.as_object()
			.map(|o| o.to_owned())
			.unwrap()
	}
}

#[derive(Debug)]
pub struct File {
	pub name: String,
	pub path: PathBuf,
	pub fullpath: PathBuf,
	pub dir: PathBuf,

	pub rootpath: String,
	pub nestlevel: usize,
	engine_types: Vec<EngineType>,

	pub dest_name: String,
	pub dest_url: String,

	pub meta: RwLock<Arc<FileMeta>>,
	pub page: RwLock<BTreeMap<String, Box<PluginPageData + Send + Sync>>>,
}

impl File {
	pub fn new<P1, P2>(p: P1, basepath: P2) -> Result<File>
		where P1: AsRef<Path>,
			  P2: AsRef<Path>
	{
		let p = p.as_ref();
		let basepath = basepath.as_ref();

		let rel_path = p.strip_prefix("./").unwrap_or(p);

		let dir = p.parent()
			.map(|dir| dir.strip_prefix("./").unwrap_or(dir).into())
			.unwrap_or_else(|| PathBuf::from("./"));

		let file_name = p.file_name()
			.and_then(|v| v.to_str())
			.unwrap_or_else(|| "")
		;

		let (dest_name, engine_pipeline) = File::detect_extensions(file_name);
		let url = dir.join(&dest_name);

		let rel_dir_count = dir.components().count();

		let mut rel_dir = String::new();
		for _ in 0..rel_dir_count {
			rel_dir.push_str("../");
		}

		let fullpath = basepath.join(rel_path);

		let file_type = if (&dest_name).ends_with(".html") || (&dest_name).ends_with(".htm") {
			if dest_name.starts_with("index.") {
				FileType::Index
			} else {
				FileType::Page
			}
		} else {
			FileType::File
		};

		let meta = FileMeta {
			title: file_name.to_string(),
			name: dest_name.clone(),
			url: format!("/{}", url.to_string_lossy().into_owned()),
			tags: Vec::new(),
			date: Date::empty(),
			// TODO: take file date
			ty: "".into(),
			file_type,
			author: "".into(),
			dir: dir.to_str().unwrap_or_else(|| "").to_owned(),
			rootpath: rel_dir.clone(),
			caption: "".into(),
			extra: Object::new(),
		};


		Ok(File {
			name: meta.title.clone(),
			fullpath,
			path: rel_path.into(),
			dir: dir.into(),
			rootpath: rel_dir,
			nestlevel: rel_dir_count,
			engine_types: engine_pipeline,

			dest_name,
			dest_url: meta.url.clone(),

			meta: RwLock::new(Arc::new(meta)),
			page: RwLock::new(BTreeMap::new()),
		})
	}

	pub fn new_index<P1, P2>(dir: P1, basepath: P2) -> Result<File>
		where P1: AsRef<Path>,
			  P2: AsRef<Path>
	{
		let dir = dir.as_ref();
		let basepath = basepath.as_ref();

		let rel_path = dir.strip_prefix("./").unwrap_or(dir);

		let dest_name = "index.html".to_string();
		let url = dir.join(&dest_name);

		let rel_dir_count = dir.components().count() - 1;

		let mut rel_dir = String::new();
		for _ in 0..rel_dir_count {
			rel_dir.push_str("../");
		}

		let fullpath = basepath.join(rel_path);

		let meta = FileMeta {
			title: "Index".to_string(),
			name: "index.html".to_string(),
			url: format!("/{}", url.to_string_lossy().into_owned()),
			tags: Vec::new(),
			date: Date::empty(),
			ty: "index".into(),
			file_type: FileType::GeneratedIndex,
			author: "".into(),
			dir: dir.to_str().unwrap_or_else(|| "").to_owned(),
			rootpath: rel_dir.clone(),
			caption: "".into(),
			extra: Object::new(),
		};

		Ok(File {
			name: meta.title.clone(),
			fullpath,
			path: rel_path.into(),
			dir: dir.into(),
			rootpath: rel_dir,
			nestlevel: rel_dir_count,
			engine_types: vec![],

			dest_name,
			dest_url: meta.url.clone(),

			meta: RwLock::new(Arc::new(meta)),
			page: RwLock::new(BTreeMap::new()),
		})
	}

	pub fn id(&self) -> String {
		format!("file-{}", &self.fullpath.display())
	}

	pub fn name(&self) -> String {
		self.name.clone()
	}

	pub fn file_type(&self) -> FileType {
		self.meta.read().unwrap().file_type.clone()
	}

	pub fn is_page(&self) -> bool {
		let file_type = self.file_type();
		file_type == FileType::Page
			|| file_type == FileType::GeneratedIndex
			|| file_type == FileType::Index
	}

	pub fn is_index(&self) -> bool {
		self.file_type() == FileType::Index
	}

	pub fn is_generated_index(&self) -> bool {
		self.file_type() == FileType::GeneratedIndex
	}

	pub fn parse_meta(&self, config: &Config, engines: &Engines) -> Result<()> {
		if self.is_generated_index() {
			return Ok(());
		}

		if self.engine_types.is_empty() {
			trace!("No engine defined for {}", self.fullpath.display());
			return Ok(());
		}

		trace!("Processing {}", self.fullpath.display());
		let contents = files::read_part(
			Path::new(&self.fullpath),
			8192
		)?;

		if contents.len() >= 4 && contents[0..4] != [b'-', b'-', b'-', b'\n'] {
			trace!("No YAML prelude available in {}", self.fullpath.display());
			return Ok(());
		}

		let contents = String::from_utf8_lossy(&contents);
		let contents: Vec<&str> = contents.split("---\n").collect();

		// contents[0] is always "", as the file starts with "---\n"
		// So the real prelude is at index 1, and the page contents is at index 2
		if contents.len() < 2 || contents[1].is_empty() {
			warn!("Missing YAML prelude in {}", self.fullpath.display());
			return Ok(());
		}

		let prelude = "---\n".to_string() + contents[1] + "\n";

		let ts_yaml_start = time::get_time();

		let file_data = yaml_from_str::<Value>(&prelude)
			.chain_err(|| ErrorKind::Model("Page".into(), format!("Unable to load YAML from {}", self.fullpath.display())))?;

		let mut file_data = file_data.as_object()
			.ok_or_else(|| ErrorKind::Model("Page".into(), format!("Page '{}' prelude must be a valid key-value map", self.name())))?
			.clone();

		let ts_yaml_end = time::get_time();

		print::tprofiling(&format!("YAML parsing for {}", self.dest_url), &ts_yaml_start, &ts_yaml_end);

		let mut caption = Self::meta_caption(&mut file_data, &contents).unwrap_or_else(|| "".into());
		let data = Object::new();

		for engine_type in &self.engine_types {
			let new_caption = engines.render_caption(engine_type, &caption, &data);
			if new_caption.is_err() {
				warn!("Error rendering caption for {}", self.name);
			} else {
				// unwrap is fine as we already check if is_err()
				let new_caption = new_caption.unwrap();
				if !new_caption.is_empty() {
					caption = new_caption;
				}
			}
		}
		let file_type = self.file_type();

		// unwrap is a little dangerous here, but it's hard to recover the state. Better panic.
		*(self.meta.write().unwrap()) = Arc::new(FileMeta {
			title: Self::meta_title(&mut file_data).unwrap_or_else(|| "No title".to_string()),
			name: self.dest_name.clone(),
			url: self.dest_url.clone(),
			tags: Self::meta_tags(&mut file_data),
			date: Self::meta_date(&mut file_data).unwrap_or_else(|| {
				warn!("Warning: no date defined for {}", self.fullpath.display());
				Date::empty()
			}),
			ty: Self::meta_doctype(&mut file_data).unwrap_or_else(|| "page".to_string()),
			file_type,
			author: Self::meta_author(&mut file_data).unwrap_or_else(|| config.pages.author.clone()),
			dir: self.dir.to_str().unwrap_or_else(|| "").to_owned(),
			rootpath: self.rootpath.clone(),
			caption: caption,
			extra: file_data,
		});

		Ok(())
	}

	pub fn render<P>(&self, config: &Config, engines: &Engines, dest_path: &P, site_meta: &Object) -> Result<()>
		where P: AsRef<Path>
	{
		let dest_path = dest_path.as_ref();
		let dest_file = dest_path.join(&self.dest_url[1..]);

		let mut contents;
		if self.is_generated_index() {
			contents = "".to_string();
		} else {
			if self.engine_types.is_empty() {
				// Nothing to do, just copy the file
				files::copy_file(&self.fullpath, &dest_file)?;
				return Ok(());
			}

			contents = files::read_to_string(Path::new(&self.fullpath))?;
		}

		let ts_start = time::get_time();

		// If the file has YAML prelude, skip the prelude
		if contents.starts_with("---\n") {
			let pos = contents[1..].find("---\n").map(|res| res + 4).unwrap_or(0);
			contents = (&contents)[pos..].into();
		}

		let meta = self.get_meta();
		let mut meta_object = self.get_meta_object();
		let theme_path = format!("{}{}/", &meta.rootpath, &config.theme.dest);
		let mut theme = Object::new();
		theme.insert("name".into(), Value::String(config.theme.name.clone()));
		theme.insert("path".into(), Value::String(theme_path));
		theme.insert("data".into(), Value::Object(config.theme.data.clone()));

		meta_object.insert("page".into(), Value::Object(self.get_page_meta_object()));
		meta_object.insert("site".into(), Value::Object(site_meta.to_owned()));
		meta_object.insert("theme".into(), Value::Object(theme));

		for engine_type in &self.engine_types {
			contents = engines.render_body(engine_type, &contents, &meta_object)
				.chain_err(|| ErrorKind::Model(
					"File".into(),
					format!("Error rendering body of {}, {:?}", self.name, meta).into())
				)?;
		}

		if self.is_page() && &meta.ty != "none" {
			let final_engine = EngineType::from(&config.templates.engine)
				.or(EngineType::Tera);

			if engines.has_template(&final_engine, &meta.ty) {
				meta_object.insert("body".into(), Value::String(contents));
				contents = engines.render_page(&final_engine, &meta.ty, &meta_object)
					.chain_err(|| ErrorKind::Model(
						"File".into(),
						format!("Error rendering {}, {:?}", self.name, meta).into())
					)?;
			} else {
				warn!(format!("Template '{}' not found. Missing theme?", &meta.ty));
			}
			files::write_to_file(&dest_file, &contents)?;

			let ts_end = time::get_time();
			print::tprofiling("Rendering", &ts_start, &ts_end);
		}

		Ok(())
	}

	// unwrap is a little dangerous here, but it's hard to recover the state. Better panic.
	pub fn get_meta(&self) -> Arc<FileMeta> {
		self.meta.read().unwrap().clone()
	}

	// meta must be serializable to an object, or we have bigger problems than this panic()ing
	pub fn get_meta_object(&self) -> Object {
		to_value(self.get_meta())
			.unwrap()
			.as_object()
			.map(|o| o.to_owned())
			.unwrap()
	}

	pub fn add_page_data(&self, name: &str, val: Box<PluginPageData + Send + Sync>) {
		// unwrap is a little dangerous here, but it's hard to recover the state. Better panic.
		let mut page_meta = self.page.write().unwrap();
		page_meta.insert(name.into(), val);
	}

	// meta must be serializable to an object, or we have bigger problems than this panic()ing
	pub fn get_page_meta_object(&self) -> Object {
		// unwrap is a little dangerous here, but it's hard to recover the state. Better panic.
		let page = self.page.read().unwrap();
		to_value(&*page)
			.unwrap()
			.as_object()
			.map(|o| o.to_owned())
			.unwrap()
	}

	fn meta_title(file_data: &mut Object) -> Option<String> {
		file_data.remove("title").and_then(|e| e.as_str().map(|v| v.to_string()))
	}

	fn meta_doctype(file_data: &mut Object) -> Option<String> {
		file_data.remove("type")
			.and_then(|e| e.as_str().map(|v| v.to_string()))
			.and_then(|v| if v.is_empty() { None } else { Some(v) })
	}

	fn meta_author(file_data: &mut Object) -> Option<String> {
		file_data.remove("author")
			.and_then(|e| e.as_str().map(|v| v.to_string()))
			.and_then(|v| if v.is_empty() { None } else { Some(v) })
	}

	fn meta_caption(file_data: &mut Object, contents: &[&str]) -> Option<String> {
		let mut meta_caption = file_data.remove("caption")
			.and_then(|e| e.as_str().map(|v| v.to_string()))
			.unwrap_or_default();

		if meta_caption.is_empty() {
			meta_caption.push_str(
				&contents[2]
					.find("<!--more-->")
					.or_else(|| contents[2].find("\n\n"))
					.and_then(|l| Some(contents[2][..l].to_string()))
					.unwrap_or_else(|| "".to_string())
			);
		}

		Some(meta_caption)
	}

	fn meta_tags(file_data: &mut Object) -> Vec<String> {
		file_data.remove("tags")
			.and_then(|e| e.as_array().cloned())
			.unwrap_or(Vec::new())
			.iter()
			.map(|x| x.as_str().unwrap_or_else(|| "").to_string())
			.collect()
	}

	fn meta_date(file_data: &mut Object) -> Option<Date> {
		let found_date = file_data
			.remove("date")
			.and_then(|e| e.as_str().map(|v| v.to_string()))
			.unwrap_or_default();
		match DATEVAL_RE.captures(&found_date) {
			Some(cap) => {
				Some(Date::new(
					cap.name("y").map(|v| v.as_str()).unwrap_or("0000").to_string(),
					cap.name("m").map(|v| v.as_str()).unwrap_or("00").to_string(),
					cap.name("d").map(|v| v.as_str()).unwrap_or("00").to_string(),
					cap.name("h").map(|v| v.as_str()).unwrap_or("00").to_string(),
					cap.name("i").map(|v| v.as_str()).unwrap_or("00").to_string(),
					cap.name("s").map(|v| v.as_str()).unwrap_or("00").to_string(),
				))
			}
			None => {
				None
			}
		}
	}

	fn detect_extensions<P>(name: P) -> (String, Vec<EngineType>)
		where P: AsRef<Path>
	{
		let name = name.as_ref().to_string_lossy().to_string();
		let engine_extensions = Engines::extensions();
		let extensions = name.split('.').skip(1).map(|v| v.to_owned()).collect::<Vec<_>>();

		let mut new_extensions = Vec::new();
		let mut l = name.len();

		let engine_pipeline = extensions.into_iter().rev()
			.take_while(|ext| engine_extensions.contains_key(ext))
			.map(|ext| {
				// Keep track of how many chars to remove from end of filename due to extension
				l -= ext.len() + 1;

				// Get the engine type and optionally the new file extension to append
				// unwrap is fine as we already checked if it contains_key() earlier
				let res = &engine_extensions[&ext];
				if let Some(ref new_ext) = res.1 {
					new_extensions.push(new_ext.clone());
				}
				res.0.clone()
			})
			.collect::<Vec<EngineType>>()
		;

		let mut new_name = name.clone();
		new_name.truncate(l);
		for ext in new_extensions.into_iter().rev() {
			new_name = format!("{}.{}", new_name, ext);
		}

		(new_name, engine_pipeline)
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_detect_extensions() {
		let (name, types) = File::detect_extensions(Path::new("file.md"));
		assert_eq!(vec![EngineType::Markdown], types);
		assert_eq!("file.html", &name);

		let (name, types) = File::detect_extensions(Path::new("file.md.tera"));
		assert_eq!(vec![EngineType::Tera, EngineType::Markdown], types);
		assert_eq!("file.html", &name);

		let (name, types) = File::detect_extensions(Path::new("file.tera.md"));
		assert_eq!(vec![EngineType::Markdown, EngineType::Tera], types);
		assert_eq!("file.html", &name);

		let (name, types) = File::detect_extensions(Path::new("file.md.twig"));
		assert_eq!(vec![EngineType::Tera, EngineType::Markdown], types);
		assert_eq!("file.html", &name);

		let (name, types) = File::detect_extensions(Path::new("file.md.j2"));
		assert_eq!(vec![EngineType::Tera, EngineType::Markdown], types);
		assert_eq!("file.html", &name);

		let (name, types) = File::detect_extensions(Path::new("file.md.jinja2"));
		assert_eq!(vec![EngineType::Tera, EngineType::Markdown], types);
		assert_eq!("file.html", &name);

		let (name, types) = File::detect_extensions(Path::new("file.md.hbs"));
		assert_eq!(vec![EngineType::Hbs, EngineType::Markdown], types);
		assert_eq!("file.html", &name);

		let (name, types) = File::detect_extensions(Path::new("file.hbs"));
		assert_eq!(vec![EngineType::Hbs], types);
		assert_eq!("file", &name);

		let (name, types) = File::detect_extensions(Path::new("file.html"));
		assert_eq!(Vec::<EngineType>::new(), types);
		assert_eq!("file.html", &name);

		let (name, types) = File::detect_extensions(Path::new("file.md.html"));
		assert_eq!(Vec::<EngineType>::new(), types);
		assert_eq!("file.md.html", &name);

		let (name, types) = File::detect_extensions(Path::new("file.tera.html"));
		assert_eq!(Vec::<EngineType>::new(), types);
		assert_eq!("file.tera.html", &name);

		let (name, types) = File::detect_extensions(Path::new("file.twig.html"));
		assert_eq!(Vec::<EngineType>::new(), types);
		assert_eq!("file.twig.html", &name);

		let (name, types) = File::detect_extensions(Path::new("file"));
		assert_eq!(Vec::<EngineType>::new(), types);
		assert_eq!("file", &name);

		let (name, types) = File::detect_extensions(Path::new("tera"));
		assert_eq!(Vec::<EngineType>::new(), types);
		assert_eq!("tera", &name);

		let (name, types) = File::detect_extensions(Path::new("twig"));
		assert_eq!(Vec::<EngineType>::new(), types);
		assert_eq!("twig", &name);

		let (name, types) = File::detect_extensions(Path::new("hbs"));
		assert_eq!(Vec::<EngineType>::new(), types);
		assert_eq!("hbs", &name);

		let (name, types) = File::detect_extensions(Path::new("md"));
		assert_eq!(Vec::<EngineType>::new(), types);
		assert_eq!("md", &name);

//		let (name, types) = File::detect_extensions(Path::new("fileなまえ.md.less.twig"));
//		assert_eq!(vec![EngineType::Tera, EngineType::Less, EngineType::Markdown], types);
//		assert_eq!("fileなまえ.html.js", &name);
	}
}
