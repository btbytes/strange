/*
	Copyright 2017 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

mod directory;
mod file;

pub use self::directory::Directory;
pub use self::file::File;
pub use self::file::FileMeta;

use serde::ser::{Serialize, Serializer, SerializeStruct};
use serde_json::{Map as JsonMap, Value};

type Object = JsonMap<String, Value>;

#[derive(Debug, Clone)]
pub struct Date {
	pub year: String,
	pub month: String,
	pub day: String,
	pub hour: String,
	pub minute: String,
	pub second: String,
}

impl Serialize for Date {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
		where S: Serializer
	{
		// 3 is the number of fields in the struct.
		let mut state = serializer.serialize_struct("Date", 12)?;
		state.serialize_field("year", &self.year)?;
		state.serialize_field("month", &self.year)?;
		state.serialize_field("day", &self.year)?;
		state.serialize_field("hour", &self.year)?;
		state.serialize_field("minute", &self.year)?;
		state.serialize_field("second", &self.year)?;

		state.serialize_field("iso", &self.iso())?;
		state.serialize_field("full", &self.full())?;
		state.serialize_field("datetime", &self.datetime())?;
		state.serialize_field("date", &self.date())?;
		state.serialize_field("time", &self.time())?;
		state.serialize_field("fulltime", &self.fulltime())?;

		state.end()
	}
}


//impl ToJson for Date {
impl Date {
	pub fn empty() -> Date {
		Date {
			year: "0000".to_string(),
			month: "00".to_string(),
			day: "00".to_string(),
			hour: "00".to_string(),
			minute: "00".to_string(),
			second: "00".to_string()
		}
	}

	pub fn new(y: String, m: String, d: String, h: String, i: String, s: String) -> Date {
		Date {
			year: y,
			month: m,
			day: d,
			hour: h,
			minute: i,
			second: s
		}
	}

	pub fn iso(&self) -> String {
		format!("{}-{}-{}T{}:{}:{}Z", self.year, self.month, self.day, self.hour, self.minute, self.second).to_string()
	}

	pub fn datetime(&self) -> String {
		format!("{}-{}-{} {}:{}", self.year, self.month, self.day, self.hour, self.minute).to_string()
	}

	pub fn full(&self) -> String {
		format!("{}-{}-{} {}:{}:{}", self.year, self.month, self.day, self.hour, self.minute, self.second).to_string()
	}

	pub fn date(&self) -> String {
		format!("{}-{}-{}", self.year, self.month, self.day).to_string()
	}

	pub fn fulltime(&self) -> String {
		format!("{}:{}:{}", self.hour, self.minute, self.second).to_string()
	}

	pub fn time(&self) -> String {
		format!("{}:{}", self.hour, self.minute).to_string()
	}
}
