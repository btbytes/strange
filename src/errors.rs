/*
	Copyright 2017 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

error_chain! {
	errors {
		Warning(msg: String) {
			description("Warning")
			display("Warning: {}", msg)
		}

		Error(msg: String) {
			description("Error")
			display("Error: {}", msg)
		}

		Model(model: String, msg: String) {
			description("Model error")
			display("Model '{}' error: {}", model, msg)
		}

		Plugin(plugin: String, msg: String ) {
			description("Plugin error")
			display("Plugin '{}' error: {}", plugin, msg)
		}

		Tpl(tpl: String, msg: String) {
			description("Template error")
			display("Template ({}) error: {}", tpl, msg)
		}

		Theme(msg: String) {
			description("Theme error")
			display("Theme error: {}", msg)
		}
	}

	foreign_links {
		Io(::std::io::Error);
		Clap(::clap::Error);
		Term(::term::Error);
		HbsRender(::handlebars::RenderError);
		HbsTpl(::handlebars::TemplateError);
		HbsTplRender(::handlebars::TemplateRenderError);
		Tera(::tera::Error);
		Json(::serde_json::Error);
		Yaml(::serde_yaml::Error);
		Hyper(::hyper::Error);
		Notify(::notify::Error);
		Channel(::std::sync::mpsc::RecvError);
	}
}

pub fn dump_error(e: &Error) {
	error!("{}", e);

	for e in e.iter().skip(1) {
		error!("caused by: {}", e);
	}

	if let Some(backtrace) = e.backtrace() {
		error!("backtrace: {:?}", backtrace);
	}
}
