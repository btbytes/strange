/*
	Copyright 2017 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::env;
use std::fs;

use errors::*;
use config::Config;

pub fn run(config: &Config) -> Result<String> {
	let dest_path = env::current_dir()?.join(&config.paths.dest);
	if dest_path.exists() {
		fs::remove_dir_all(&dest_path)
			.chain_err(|| format!("Cannot clean build directory {}", dest_path.display()))?;
	}

	fs::create_dir_all(&dest_path)
		.chain_err(|| format!("Unable to create path to file '{}'", dest_path.display()))?;

	Ok("Build cleaned".into())
}
