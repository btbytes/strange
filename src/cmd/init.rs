/*
	Copyright 2017 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::fs;
use std::env;
use std::path::Path;

use errors::*;
use config::{self, Config};
use plugin::PLUGINS;

pub fn run<P>(
	config: &Config,
	projectdir: Option<P>
) -> Result<String>
	where P: AsRef<Path>
{
	// TODO: better check if the project is already created
	if let Some(dir) = projectdir {
		let dir = dir.as_ref();
		fs::create_dir(dir).chain_err(|| {
			format!(
				"Unable to create requested project '{}'",
				dir.to_str().unwrap_or_else(|| "[invalid filename]")
			)
		})?;
		env::set_current_dir(dir)?;
	};

	let base_dir = env::current_dir()?;

	config::write_config(config)?;

	/*
	 * Create directories
	 */

	let dirs_to_create = vec![&config.paths.src, &config.paths.dest, &config.paths.themes];
	for dir_to_create in dirs_to_create {
		let full_path = base_dir.join(dir_to_create);
		if Path::new(&full_path).exists() {
			continue;
		}
		fs::create_dir_all(&full_path).chain_err(|| {
			format!(
				"Unable to create requested project directory '{}'",
				full_path.to_str().unwrap_or_else(|| "[invalid filename]")
			)
		})?;
	}

	PLUGINS.post_init(config, &base_dir)?;

	Ok("Project initialized".into())
}
