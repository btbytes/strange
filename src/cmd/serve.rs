/*
	Copyright 2017 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::env;
use std::path::Path;
use std::thread;
use time;
use hyper::server::{Http, Request, Response, Service};
use hyper::Error as HError;
use hyper::StatusCode;
use futures::future;

use errors::*;
use config::Config;
use utils::files;
use cmd::watch;

pub fn run(config: &Config) -> Result<String> {
	let address = format!("{}:{}", config.serve.ip, config.serve.port).parse().unwrap();
	let path = env::current_dir()?.join(&config.paths.dest);

	let serve_handle = thread::spawn(move || {
		info!("Listening on http://{}", address);

		let server = Http::new()
			.bind(&address, move || Ok(FileServer { path: path.as_path() }))
			.unwrap();

		server.run().unwrap();
	});

	let _ = watch::run(config);

	let _ = serve_handle.join();

	Ok("Serve ended".into())
}

struct FileServer<'a> {
	path: &'a Path,
}

impl<'a> Service for FileServer<'a> {
	type Request = Request;
	type Response = Response;
	type Error = HError;
	type Future = future::FutureResult<Self::Response, Self::Error>;

	fn call(&self, req: Request) -> Self::Future {
		let mut res = Response::new();
		self.handle_file(req, &mut res);
		future::ok(res)
	}
}

impl<'a> FileServer<'a> {
	fn handle_file(&self, req: Request, res: &mut Response) {
		let req_path = req.uri().path();

		let path = self.path.join(&req_path[1..]);
		let path = if path.is_file() {
			path
		} else {
			path.join("index.html")
		};

		if path.exists() {
			debug!(
				"{} 200 {}",
				time::strftime("[%F %T]", &time::now()).ok().unwrap_or_else(|| "".into()),
				req_path
			);
			let _ = files::read_binary(&path)
				.map_err(|e| {
					error!("{:?}", e);
					res.set_status(StatusCode::InternalServerError);
					res.set_body(format!("<h1>Error reading page {} from disk</h1>", path.display()));
				})
				.map(|body| res.set_body(body));
		} else {
			debug!(
				"{} 404 {}",
				time::strftime("[%F %T]", &time::now()).ok().unwrap_or_else(|| "".into()),
				req_path
			);
			res.set_status(StatusCode::NotFound);
			res.set_body(format!("<h1>Page {} not found</h1>", path.display()));
		}
	}
}
