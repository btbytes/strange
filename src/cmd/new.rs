/*
	Copyright 2017 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::process;
use std::path::{Path, PathBuf};
use time;
use slug;

use errors::*;
use config::Config;
use tpl;

pub fn run<P>(config: &Config, title: &str, path: P) -> Result<String>
	where P: AsRef<Path>
{
	let path = path.as_ref();

	if title == "" {
		error!("No article title given");
		process::exit(1);
	}

	let doc_dt = time::strftime("%F %H:%M:%S", &(time::now()))
		.chain_err(|| "Unable to calculate the date")?;

	let mut dest_path = PathBuf::from(&config.paths.src);
	dest_path.push(path);

	let filename = format!(
		"{}{}.md",
		&(if config.pages.date || config.pages.time {
			if config.pages.time {
				time::strftime("%F-%H%M-", &time::now()).ok()
			} else {
				time::strftime("%F-", &time::now()).ok()
			}
		} else {
			None
		}).unwrap_or_else(|| "".to_string()),
		&slug::slugify(title)
	);

	dest_path.push(filename);

	if dest_path.exists() {
		return Err(ErrorKind::Warning(
			format!(
				"File `{}` already exists",
				dest_path.display()
			).to_string()
		).into());
	}

	let engines = tpl::Engines::new(config)?;
	// We force markdown-only pages for now. Maybe in the future we'll add a flag
//	engines.write_page(&tpl::EngineType::from(&config.templates.engine), dest_path, &title, &doc_dt);
	engines.write_page(&tpl::EngineType::Markdown, dest_path, title, &doc_dt)?;

	Ok("Page created.".into())
}
