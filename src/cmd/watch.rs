/*
	Copyright 2017 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::env;
use std::path::{Path, Component};
use std::sync::mpsc::channel;
use notify::{Watcher, RecursiveMode, watcher, DebouncedEvent};
use time;
use std::time::Duration;

use errors::*;
use config::Config;
use cmd::build;
use utils;
use utils::files;

pub fn run(config: &Config) -> Result<String> {
	let (tx, rx) = channel();
	let cd = env::current_dir()?;

	// Always build at least once before starting to watch
	// Calculate how long it takes and set the watcher to double that.
	let ts_start = time::get_time();
	let _ = build::run(config)
		.or_else(|e: Error| -> Result<String> {
			dump_error(&e);
			Ok("Error rendering".into())
		});
	let ts_end = time::get_time();
	let ts_render = utils::time_diff_in_ms(&ts_start, &ts_end) * 2;

	let mut watcher = watcher(tx, Duration::from_millis(ts_render))?;
	watcher.watch(&cd, RecursiveMode::Recursive)?;

	info!("Watching for file changes");
	debug!("Set watcher interval to {}ms", ts_render);

	loop {
		rx.recv()
			.map(|event| {
				trace!("Event: {:?}", event);
				match event {
					DebouncedEvent::Create(path)
					| DebouncedEvent::Write(path) => {
						if is_path_to_process(&path, config) {
							debug!("Triggering build on {} changes", path.display());
							let _ = build::run(config)
								.or_else(|e: Error| -> Result<String> {
									dump_error(&e);
									Ok("Error rendering".into())
								});
						}
					}
					_ => {}
				}
			})?;
	}
}

fn is_path_to_process(path: &Path, config: &Config) -> bool {
	if path.is_dir() {
		return false;
	}

	let file_name: &str = path.file_name()
		.and_then(|name| name.to_str())
		.unwrap_or_else(|| "".into());

	let cwd = env::current_dir();
	if cwd.is_err() {
		return false;
	}
	let cwd = cwd.unwrap();

	let dir = match path.strip_prefix(&cwd).unwrap().components().nth(0) {
		Some(Component::Normal(name)) => name.to_str().unwrap_or_else(|| ""),
		_ => ""
	};

	!(
		dir.is_empty()
			|| files::is_filename_not_allowed(dir)
			|| dir == config.paths.dest
			|| files::is_filename_not_allowed(file_name)
	)
}
