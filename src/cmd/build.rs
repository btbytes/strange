/*
	Copyright 2017 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::env;
use std::fs;
use std::path::{Path, PathBuf};
use rayon;
use rayon::prelude::*;
use time;

use errors::*;
use utils::print;
use models::{Directory, File};
use plugin::PLUGINS;
use tpl;
use config::Config;
use cmd;
use theme;

pub fn run(config: &Config) -> Result<String> {
	info!("Building...");

	let (source_path, dest_path) = prepare_paths(config)?;
	trace!(" Src: {:?}", &source_path);
	trace!("Dest: {:?}", &dest_path);

	let engines = tpl::Engines::new(config)?;
	let index = Directory::scan_from(&source_path, config)?;

	debug!("Parallelizing on {} threads", rayon::current_num_threads());

	let mut files = Vec::new();
	generate_paths(config, &index, &dest_path, &mut files)?;
	info!("Found {} files", files.len());

	info!("Parsing meta-data");
	let ts_start = time::get_time();
	let _: Vec<Result<()>> = files.par_iter()
		.map(|f| {
			f.parse_meta(config, &engines)
				.map_err(|e| {
					dump_error(&e);
					e
				})
		})
		.collect();
	let ts_end = time::get_time();
	print::dprofiling("Parsing", &ts_start, &ts_end);

	info!("Processing plugins");
	let ts_start = time::get_time();
	let plugin_results = PLUGINS.process_docs(config, &index)?.get_data();
	let ts_end = time::get_time();
	print::dprofiling("Processing", &ts_start, &ts_end);

	info!("Rendering");
	let ts_start = time::get_time();
	let _: Vec<Result<()>> = files.par_iter()
		.map(|f| {
			f.render(config, &engines, &dest_path, &plugin_results)
				.map_err(|e| {
					dump_error(&e);
					e
				})
		})
		.collect();
	let ts_end = time::get_time();
	print::dprofiling("Rendering", &ts_start, &ts_end);

	info!("Post-processing plugins");
	let ts_start = time::get_time();
	let _ = PLUGINS.post_build(config, &index)?.get_data();
	let ts_end = time::get_time();
	print::dprofiling("Post-processing", &ts_start, &ts_end);

	let themes = theme::Themes::new(config)?;

	info!("Processing theme");
	let ts_start = time::get_time();
	match themes.get_current() {
		Some(theme) => {
			theme.render(&plugin_results).map_err(|e| Error::with_chain(e, ErrorKind::Theme("Unable to render theme".into())))?;
		}
		None => {
			warn!("No theme selected in the config file")
		}
	}
	let ts_end = time::get_time();
	print::dprofiling("Processing", &ts_start, &ts_end);

	Ok("Build finished.".into())
}

fn prepare_paths(config: &Config) -> Result<(PathBuf, PathBuf)> {
	cmd::clean::run(config)?;

	let source_path = env::current_dir()?.join(&config.paths.src);
	let dest_path = env::current_dir()?.join(&config.paths.dest);

	if dest_path.starts_with("./") {
		dest_path.strip_prefix("./")
			.chain_err(|| format!("Unable to strip ./ from path to file '{}'", dest_path.display()))?;
	}

	Ok((source_path, dest_path))
}

fn generate_paths<'index, P>(config: &Config, dir: &'index Directory, dest_path: P, files: &mut Vec<&'index File>) -> Result<()>
	where P: AsRef<Path>
{
	let dest_path = dest_path.as_ref();
	let dest = dest_path.join(&dir.path);
	trace!("creating dir {:?}", dest);

	fs::create_dir_all(&dest)
		.chain_err(|| format!("Unable to create path  '{:?}'", dest).to_string())?;

	for dir in &dir.dirs {
		generate_paths(config, dir, dest_path, files)?;
	}

	for file in &dir.files {
		files.push(file);
	}

	Ok(())
}
