# Strange configuration file
site:
  # The title of the site. It will be used in the <title> and possibly in some plugins
  title: {{ site.title }}

  # The URL of the website. Do not append "/index.html" at the end.
  url: {{ site.url }}

  # A short description of the website
  description: {{ site.description }}

  # Whether to create index pages automatically (experimental)
  indexes: {{ site.indexes }}

templates:
  # Select the templating engine. Available: `tera`, `handlebars`. Default: `tera`
  engine: {{ templates.engine }}

pages:
  # If set to true, automatically prepend date to pages' filename, like `strange new -d`. Default: `false`
  date: {{ pages.date }}

  # If set to true, automatically prepend time to pages' filename, like `strange new -dt`. Default: `false`
  time: {{ pages.time }}

  # The author name to use if it's not defined in the pages' metadata. Default: `Unknown`
  author: {{ pages.author }}

paths:
  # Sets the path to the folder containing source files. Default: `src`
  src: {{ paths.src }}

  # Sets the path to the destination where pages will be built files. Default: `build`
  dest: {{ paths.dest }}

  # Sets the path to the folder containing the downloaded themes. Default: `themes`
  themes: {{ paths.themes }}

serve:
  # Sets the IP address on which the `strange serve` command will start a webserver. Default: `127.0.0.1`
  ip: {{ serve.ip }}

  # Sets the port on which the webserver will listen
  port: {{ serve.port }}

# Theme configuration (experimental)
theme:
  # The name of the theme to use. You can have multiple themes downloaded in `themes`. Default: `default`
  name: {{ theme.name}}

  # The directory that will be created inside the destination (`paths.dest`) to contain the compiled theme files. Default: `theme`
  dest: {{ theme.dest }}

  # Optional theme configurations. Default: empty
  options:
    {{ theme.options|yaml_encode(indent=4) }}

  # Data that will be passed to the javascript parts of the compiled theme (e.g.: menu layout). Default: empty
  data:
    {{ theme.data|yaml_encode(indent=4) }}

plugins:
  {{ plugins|yaml_encode(indent=2) }}
