/*
	Copyright 2017 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use serde_json::{Map as JsonMap, Value};
use std::sync::Arc;
use std::collections::BTreeMap;
use std::path::Path;
use std::env;

use errors::*;
use config;
use utils::files;

pub mod engine_tera;
pub mod engine_handlebars;
pub mod engine_pulldown;

type Object = JsonMap<String, Value>;

#[derive(Clone, Debug, PartialEq)]
pub enum EngineType {
	Tera,
	Hbs,
	Markdown,

	// For future expansion. Unsupported for now:
	// Less,
	// Sass,

	// Engine not detected
	None,
}

impl<'a, S> From<S> for EngineType
	where S: AsRef<str>
{
	fn from(name: S) -> EngineType {
		match name.as_ref() {
			"tera" => EngineType::Tera,
			"hbs" | "handlebars" => EngineType::Hbs,
			"md" | "markdown" => EngineType::Markdown,

			_ => EngineType::None,
		}
	}
}

impl EngineType {
	pub fn or(self, ty: EngineType) -> EngineType {
		if self == EngineType::None {
			ty
		} else {
			self
		}
	}
}

pub struct Engines {
	tera: engine_tera::Tera,
	hbs: engine_handlebars::Handlebars,
	pulldown: engine_pulldown::Pulldown,
}

impl Engines {
	pub fn new(config: &config::Config) -> Result<Arc<Engines>> {
		let tpl_dir = Path::new(&config.paths.themes)
			.join(&config.theme.name)
			.join("templates");
		let tpl_path = env::current_dir()?.join(&tpl_dir);

		Ok(Arc::new(Engines {
			tera: engine_tera::Tera::new(&tpl_path)?,
			hbs: engine_handlebars::Handlebars::new(&tpl_path)?,
			pulldown: engine_pulldown::Pulldown::new(config)?,
		}))
	}

	pub fn write_page<P>(&self, ty: &EngineType, dest: P, title: &str, doc_dt: &str) -> Result<()>
		where P: AsRef<Path>
	{
		let layout = match *ty {
			EngineType::Markdown => self.pulldown.get_page_src(title, doc_dt),
			EngineType::Tera => self.tera.get_page_src(title, doc_dt),
			EngineType::Hbs => self.hbs.get_page_src(title, doc_dt),
			_ => unimplemented!()
		};

		files::write_to_file(dest, &layout)
	}


	pub fn render_caption(&self, ty: &EngineType, contents: &str, data: &Object) -> Result<String> {
		match *ty {
			EngineType::Tera => self.tera.render_caption(contents, data),
			EngineType::Hbs => self.hbs.render_caption(contents, data),
			EngineType::Markdown => self.pulldown.render(contents),
			_ => Ok(contents.to_owned())
		}
	}

	pub fn render_body(&self, ty: &EngineType, contents: &str, data: &Object) -> Result<String> {
		match *ty {
			EngineType::Tera => self.tera.render_body(contents, data),
			EngineType::Hbs => self.hbs.render_body(contents, data),
			EngineType::Markdown => self.pulldown.render(contents),
			_ => Ok(contents.to_owned())
		}
	}

	pub fn render_page(&self, ty: &EngineType, tpl: &str, data: &Object) -> Result<String> {
		match *ty {
			EngineType::Tera => self.tera.render_page(tpl, data),
			EngineType::Hbs => self.hbs.render_page(tpl, data),
			_ => Err("Invalid engine selected".into())
		}
	}

	pub fn has_template(&self, ty: &EngineType, tpl: &str) -> bool {
		match *ty {
			EngineType::Tera => self.tera.has_template(tpl),
			EngineType::Hbs => self.hbs.has_template(tpl),
			_ => false
		}
	}

	pub fn extensions() -> BTreeMap<String, (EngineType, Option<String>)> {
		let mut res = BTreeMap::new();
		for ext in engine_tera::Tera::extensions() {
			res.insert(ext.to_string(), (EngineType::Tera, None));
		}
		for ext in engine_handlebars::Handlebars::extensions() {
			res.insert(ext.to_string(), (EngineType::Hbs, None));
		}
		for ext in engine_pulldown::Pulldown::extensions() {
			res.insert(ext.to_string(), (EngineType::Markdown, Some("html".into())));
		}

		//		for ext in engine_lesscss::Lesscss::extensions() {
		//			res.insert(ext.to_string(), (EngineType::Less, Some("css".into())));
		//		}
		//
		//		for ext in engine_sass::Sass::extensions() {
		//			res.insert(ext.to_string(), (EngineType::Sass, Some("css".into())));
		//		}

		res
	}
}
