/*
	Copyright 2017 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use pulldown_cmark::Parser;
use pulldown_cmark::{Options, OPTION_ENABLE_TABLES, OPTION_ENABLE_FOOTNOTES};
use pulldown_cmark::html;

use errors::*;
use config::Config;

pub struct Pulldown;

impl Pulldown {
	pub fn extensions() -> Vec<&'static str> {
		vec![
			"md",
			"mkd",
			"markdown"
		]
	}

	pub fn new(_: &Config) -> Result<Pulldown> {
		Ok(Pulldown)
	}

	pub fn render(&self, md: &str) -> Result<String> {
		let mut opts = Options::empty();
		opts.insert(OPTION_ENABLE_TABLES);
		opts.insert(OPTION_ENABLE_FOOTNOTES);

		let mut s = String::with_capacity(md.len() * 2);
		let p = Parser::new_ext(md, opts);

		html::push_html(&mut s, p);

		Ok(s)
	}

	pub fn get_page_src(&self, title: &str, dt: &str) -> String {
		format!(include_str!("files/page.md"), title = title, date = dt)
	}
}
